package validate

import (
	"fmt"
	"strings"

	"bitbucket.org/jsmonet/rishi/pkg/nr"
	"github.com/pkg/errors"
)

// Namecheck consumes a string fed by the cli option -p or --provider, lowercases the string, and makes sure it reads 'newrelic' or 'datadog'
func NameCheck(name string) (formattedName string, error error) {
	formattedName = strings.ToLower(name)

	switch formattedName {
	case "newrelic", "datadog":

	default:
		error = errors.New("please enter only NewRelic or Datadog. This is case-insensitive")
	}
	return

}

// OutSelectCheck consumes the string fed to the cli arg --out or -o, lowercases the string, and validates that it reads either stdout or api, which triggers the output to return as marshalled json via another func.
func OutSelectCheck(output string) (formattedOutput string, err error) {
	formattedOutput = strings.ToLower(output)

	switch formattedOutput {
	case "stdout", "api":

	default:
		err = errors.New("only possible values are stdout and api")
	}
	return
}

// HasInstances consumes a var with the ''DataBag" type, a target string, api key string, and a bool where true only returns APM instances with child elements and false returns everything
func HasInstances(bag nr.DataBag, targetUrl string, apiKey string, onlyValid bool) {
	for i := range bag.Items {
		instances := nr.AiGather(targetUrl, apiKey, bag.Items[i].Id)
		if len(instances.NestFlds) == 0 {
			if !onlyValid {
				fmt.Printf("%v named %v has no instances\n", bag.Items[i].Id, bag.Items[i].Name)
			}
		} else {
			for j := range instances.NestFlds {
				// this is inelegant. I should grab Campoy's tree code to nest these as children to a single print of the top level ID
				fmt.Printf("%v named %v has instance id: %v named %v\n", bag.Items[j].Id, bag.Items[j].Name, instances.NestFlds[j].Id, instances.NestFlds[j].AppName)
			}
		}
	}
}
