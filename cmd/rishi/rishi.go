package rishi

import (
	"fmt"

	"bitbucket.org/jsmonet/rishi/pkg/validate"
	flag "github.com/spf13/pflag"

	"bitbucket.org/jsmonet/rishi/pkg/nr"
)

var (
	optype      = flag.StringP("type", "t", "applist", "which operation do you want to run? choices are \n\tapplist - list all applications in NR\n\tshowinstances - show specific instance information based on an app ID\n\tvalidatenr - check all app ID for child instances\n\tsynthpolicies - list all Synthetics policies for a given apiKey\n\tsynthconditions - list all Synthetic Alert Conditions by Synthetic Policy ID\n\tsynthviolations - show all active NR violations\n")
	provider    = flag.StringP("provider", "p", "", "Pick a provider between NewRelic and Datadog. Shorthand for NewRelic is 'n' and Datadog is 'd'.")
	apiKey      = flag.StringP("apikey", "a", "", "Enter the Api key to use")
	appID       = flag.IntP("appid", "i", 0, "Application ID to gather App Instance Lists or show a specific application")
	output      = flag.StringP("out", "o", "stdout", "Optional: specify stdout or api to print to command line or ship to an api endpoint")
	onlyValid   = flag.Bool("onlyvalid", false, "when used with --validatenr, --onlyvalid hides invalid responses")
	onlyNoNames = flag.Bool("onlynonames", false, "when used with listapps, this only returns apps that have no name")
)

var nrTarget string = "https://api.newrelic.com/v2"

func main() {

	flag.Parse()

	outputSelection, oErr := validate.OutSelectCheck(*output)
	if oErr != nil {
		panic(oErr)
	}

	providerName, err := validate.NameCheck(*provider)
	if err != nil {
		panic(err)
	}

	operation := map[string]func(provider string, outSelect string){
		"applist":         applist,
		"showinstances":   showInstances,
		"validatenr":      validateNr,
		"synthpolicies":   synthPolicies,
		"synthconditions": synthConditions,
		"synthalerts":     synthAlerts,
		"synthviolations": synthViolations,
	}

	operation[*optype](providerName, outputSelection)

}

func applist(providerName string, outSelect string) {
	if providerName == "newrelic" {
		//appListTarget := fmt.Sprintf("%v/applications.json", nrTarget)
		bag := nr.GatherPaginated(nrTarget, *apiKey)
		if outSelect == "stdout" {
			nr.ListAppsStdOut(bag, *onlyNoNames)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}
	}
}

func showInstances(providerName string, outSelect string) {
	if providerName == "newrelic" {
		bag := nr.AiGather(nrTarget, *apiKey, *appID)
		if outSelect == "stdout" {
			nr.ShowInstancesStdOut(bag)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}
	}
}

// validateNr seems useless now that I've looked into APM a bit more. This is a false trail and I may delete the func
func validateNr(providerName string, outSelect string) {
	if providerName == "newrelic" {
		bag := nr.GatherPaginated(nrTarget, *apiKey)
		validate.HasInstances(bag, nrTarget, *apiKey, *onlyValid)
	}
}

func synthPolicies(providerName string, outSelect string) {
	if providerName == "newrelic" {
		bag := nr.SynthPolGather(nrTarget, *apiKey)
		if outSelect == "stdout" {
			nr.ShowSynthPolicies(bag)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}
	}
}

func synthConditions(providerName string, outSelect string) {
	if providerName == "newrelic" {
		if outSelect == "stdout" {
			nr.ShowAllSynthConditions(nrTarget, *apiKey)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}

	}
}

func synthAlerts(providerName string, outSelect string) {
	if providerName == "newrelic" {
		if outSelect == "stdout" {
			nr.ShowAllSynthAlerts(nrTarget, *apiKey)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}
	}
}

func synthViolations(providerName string, outSelect string) {
	if providerName == "newrelic" {
		if outSelect == "stdout" {
			nr.ShowAllSynthViolations(nrTarget, *apiKey)
		} else if outSelect == "api" {
			fmt.Println("api output not yet written")
		}
	}
}
