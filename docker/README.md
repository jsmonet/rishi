# Dockerized!

## Basic
This dockerfile is very basic. It depends on you building rishiatrest.go into ${reporoot}/docker/rishiatrest, where this dockerfile exists at ${reporoot}/docker/Dockerfile, and you cross-compiled the binary using the argument `GOOS=linux`. You can add any other flags you want in creating the binary, but keep in mine that this has only been tested with the simple GOOS argument. 

## Building the binary

while in the reporoot, build the binary into the docker directory:

```SHELL
GOOS=linux go build -o docker/rishiatrest cmd/rishiatrest/rishiatrest.go
```

## Building the container

This is a very basic Dockerfile. It installs `ca-certificates` to support the remote calls to an https API endpoint at NewRelic and copies the binary from its same directory because `docker build` does not really like backing out of the current directory and copying files from a location outside of its own root. The common organization of a Go project would place this directory at the same level as the `bin` directory, where you typically build the binaries to. Docker's build process requirements are the only reason you build to ${reporoot}/docker/ to support the build. 

Enough pratteling, let's build it:

```SHELL
docker build . -t rishi:yourtag
```


## Networking
`Expose 8090`

This exposes the port inside the docker/kubernetes network, but does not publish the port outside of the container network. To run this locally and access it from the host, be sure to add the `-p` argument to your run statement and publish/forward 8090 to your preferred port. 

## Run it locally 

As previously mentioned, this image can be run locally and accessed by the host by running it daemonized and published:

```SHELL
docker container run -d -p 8090:8090 rishi:yourtag
```

The tag `yourtag` is simply matching the arbitrary tag placed above in the build example. 

With this running locally as shown above, you can curl the container's help endpoint (healthz to come) by curling `localhost` as such:

```SHELL
curl 'http://localhost:8090/help'
```

You can currently access the APM app list process by curling the URI `nr/applist/${apiKey}`

```SHELL
curl 'http://localhost:8090/nr/applist/e999999999e99999129999a1b6f999a297f6666ce1efcfe'
```

The api key above is sanitized and will not work. replace it with a real api key. 

At present I have not added any `?pretty` printing method, so you will need to pipe to `jq` for pretty printing and easier human reading. 

## Redirecting output to another container

This functionality is coming, and the documentation will reside in the primary README in the reporoot.