package nr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/jsmonet/rishi/pkg/core"
)

type SynthPolResponse struct {
	Policies []struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	} `json:"policies"`
}

type SynthCondResponse struct {
	SynthConds []struct {
		Id         int    `json:"id"`
		Name       string `json:"name"`
		MonitorId  string `json:"monitor_id"`
		RunbookUrl string `json:"runbook_url"`
		Enabled    bool   `json:"enabled"`
	} `json:"synthetics_conditions"`
}

type SynthAlertConditionsResponse struct {
	Conditions []struct {
		Id                  int    `json:"id,omitempty"`
		Type                string `json:"type,omitempty"`
		Name                string `json:"name,omitempty"`
		Enabled             bool   `json:"enabled,omitempty"`
		Entities            []int  `json:"entities,omitempty"`
		Metric              string `json:"metric,omitempty"`
		GcMetric            string `json:"gc_metric,omitempty"`
		RunbookUrl          string `json:"runbook_url,omitempty"`
		ConditionScope      string `json:"condition_scope,omitempty"`
		ViolationCloseTimer int    `json:"violation_close_timer,omitempty"`
		Terms               []struct {
			Duration  string `json:"duration,omitempty"`
			Operator  string `json:"operator,omitempty"`
			Priority  string `json:"priority,omitempty"`
			Threshold string `json:"threshold,omitempty"`
			Timefunc  string `json:"time_function,omitempty"`
		} `json:"terms,omitempty"`
		UserDefined struct {
			Metric    string `json:"metric,omitempty"`
			ValueFunc string `json:"value_function,omitempty"`
		}
	} `json:"conditions,omitempty"`
}

type SynthAlertViolationsResponse struct {
	Violations []struct {
		Id            int    `json:"id,omitempty"`
		Label         string `json:"label,omitempty"`
		Duration      int    `json:"duraction,omitempty"`
		PolicyName    string `json:"policy_name,omitempty"`
		ConditionName string `json:"condition_name,omitempty"`
		Priority      string `json:"priority,omitempty"`
		OpenedAt      int64  `json:"opened_at,omitempty"` // this is in epoch ms
		ClosedAt      int64  `json:"closed_at,omitempty"` // this is in epoch ms
		Entity        struct {
			Product string `json:"product,omitempty"` // example value is "Synthetics" for synth checks
			Type    string `json:"type,omitempty"`
			Name    string `json:"name,omitempty"`
		} `json:"entity,omitempty"`
		Links struct {
			PolicyId    int `json:"policy_id"`
			ConditionId int `json:"condition_id,omitempty"`
			IncidentId  int `json:"incident_id,omitempty"`
		} `json:"links,omitempty"`
	} `json:"violations,omitempty"`
}

// SynthPolGather consumes a target string and an apiKey string.
// It retrieves all the Synthetics policies relating to the given apiKey
// It then returns a SynthPolResponse struct, which maps expected json values from the response
func SynthPolGather(target string, apiKey string) (outer SynthPolResponse) {
	synthPolicyTarget := target + "/alerts_policies.json"

	rawBody := core.Grab(synthPolicyTarget, apiKey)
	outer = toJsonSynthPolResponse(rawBody)
	return
}

// SynthcondGather consumes a target string, an apiKey string, and a policyId.
// It retrieves all the Synthetics conditions relating to the given apiKey and policyIC
// It then returns a SynthCondResponse struct, which maps expected json values from the response
func SynthCondGather(target string, apiKey string, policyId int) (outer SynthCondResponse) {
	synthCondTarget := target + "/alerts_synthetics_conditions.json"
	bodyText := fmt.Sprintf(`policy_id=%v`, policyId)
	body := strings.NewReader(bodyText)
	req, _ := http.NewRequest("GET", synthCondTarget, body)
	req.Header.Set("X-Api-Key", apiKey)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)
	rawBody, _ := ioutil.ReadAll(res.Body)

	outer = toJsonSynthCondResponse(rawBody)

	return

}

func AlertCondGather(target string, apiKey string, policyId int) (outer SynthAlertConditionsResponse) {
	alertPolicyTarget := target + "/alerts_conditions.json"

	bodyText := fmt.Sprintf(`policy_id=%v`, policyId)

	body := strings.NewReader(bodyText)
	req, _ := http.NewRequest("GET", alertPolicyTarget, body)
	req.Header.Set("X-Api-Key", apiKey)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)
	rawBody, _ := ioutil.ReadAll(res.Body)

	outer = toJsonAlertCondResponse(rawBody)

	return

}

// AlertViolationGather consumes a target string and an apiKey string. It then pulls all open alert violations and unmarshals
// the response to a SynthAlertViolationsResponse var
func AlertViolationGather(target string, apiKey string) (outer SynthAlertViolationsResponse) {
	violationTarget := target + "/alerts_violations.json"

	bodyText := fmt.Sprintf(`only_open=true`) // only return open violations. Default returns all violations open and closed
	body := strings.NewReader(bodyText)

	req, _ := http.NewRequest("GET", violationTarget, body)
	//req, _ := http.NewRequest("GET", violationTarget, nil)
	req.Header.Set("X-Api-Key", apiKey)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)
	rawBody, _ := ioutil.ReadAll(res.Body)

	outer = toJsonAlertViolationResponse(rawBody)

	return
}

// toJsonSynthPolResponse unmarshals the []byte body fed to it by one of the *Gather wrapper functions.
// It then feeds the result into a SynthPolRespose var
func toJsonSynthPolResponse(rawBody []byte) (parsedBody SynthPolResponse) {
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return
}

// toJsonSynthCondResponse unmarshals the []byte body fed to it by one of the Gather wrapper functions.
// It returns a SynthCondResponse struct var.
func toJsonSynthCondResponse(rawBody []byte) (parsedBody SynthCondResponse) {
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return
}

// toJsonAlertcondResponse unmarshals the []byte body fed to it by one of the Gather wrapper functions.
// It returns a SynthAlertconditionsResponse struct var
func toJsonAlertCondResponse(rawBody []byte) (parsedBody SynthAlertConditionsResponse) {
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return
}

// toJsonAlertViolationResponse unmarshals the []byte fed to it by a Gather wrapper function and returns a SynthAlertViolationsResponse struct var
func toJsonAlertViolationResponse(rawBody []byte) (parsedBody SynthAlertViolationsResponse) {
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return
}

// ShowSynthPolicies is used to print the results of func SynthPolGather to stdout
func ShowSynthPolicies(bag SynthPolResponse) {
	for i := range bag.Policies {
		fmt.Printf("Id:\t%v\tName:\t%v\n", bag.Policies[i].Id, bag.Policies[i].Name)
	}
}

// these need to be bisected so the output can be directed wilfully

func ShowAllSynthConditions(target string, apiKey string) {
	bag := SynthPolGather(target, apiKey)
	for i := range bag.Policies {
		bag2 := SynthCondGather(target, apiKey, bag.Policies[i].Id)
		fmt.Printf("%v - %v:\n", bag.Policies[i].Id, bag.Policies[i].Name)
		for j := range bag2.SynthConds {
			fmt.Printf("Id:\t%v\tName:\t%v\tMonitor ID:\t%v\tEnabled:\t%v\n", bag2.SynthConds[j].Id, bag2.SynthConds[j].Name, bag2.SynthConds[j].MonitorId, bag2.SynthConds[j].Enabled)
		}
	}
}

func ShowAllSynthAlerts(target string, apiKey string) {
	bag := SynthPolGather(target, apiKey)
	for i := range bag.Policies {
		bag2 := AlertCondGather(target, apiKey, bag.Policies[i].Id)
		if len(bag2.Conditions) == 0 {
			fmt.Printf("%v - %v has no alerts\n", bag.Policies[i].Id, bag.Policies[i].Name)
		} else {
			for j := range bag2.Conditions {
				fmt.Printf("%v - %v has alerts", bag2.Conditions[j].Id, bag2.Conditions[j].Name)
			}
		}
	}
}

func ShowAllSynthViolations(target string, apiKey string) {
	bag := AlertViolationGather(target, apiKey)
	if len(bag.Violations) == 0 {
		fmt.Println("Everything is ok")
	} else {
		//fmt.Println("Violations found:") // basically debug
		for i := range bag.Violations {
			if bag.Violations[i].ClosedAt < 1 {
				fmt.Printf("Critical: %v - %v\tPolicy: %v - %v\tIncidentID: %v\n", bag.Violations[i].Entity.Name, bag.Violations[i].Label, bag.Violations[i].Links.PolicyId, bag.Violations[i].PolicyName, bag.Violations[i].Links.IncidentId)
			} else {
				fmt.Printf("OK: %v - %v\tPolicy: %v - %v\tIncidentID: %v\n", bag.Violations[i].Entity.Name, bag.Violations[i].Label, bag.Violations[i].Links.PolicyId, bag.Violations[i].PolicyName, bag.Violations[i].Links.IncidentId)
			}
		}
	}

}
