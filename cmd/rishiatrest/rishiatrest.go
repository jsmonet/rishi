package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"bitbucket.org/jsmonet/rishi/pkg/nr"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type AllBody struct {
	Id           int    `json:"id,omitempty"`
	Hostname     string `json:"hostname,omitempty"`
	Appname      string `json:"appname,omitempty"`
	Healthstatus string `json:"healthstatus,omitempty"`
	Enabled      bool   `json:"enabled,omitempty"`
}

var nrTarget string = "https://api.newrelic.com/v2"

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/help", getHelp).Methods("GET")
	r.HandleFunc("/nr/applist/{apiKeyPassed}", getApm).Methods("GET") // I really don't like passing it like this. TBD: figure out if this is actually a concern, possibly convert to a post
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	log.Println("listening on port 8090")
	http.ListenAndServe(":8090", loggedRouter)

}

func getHelp(w http.ResponseWriter, r *http.Request) {
	resp := "use the following extensions with a GET operation to trigger their functions"
	w.Write([]byte(resp))
}

func getApm(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	apiKey := vars["apiKeyPassed"]
	bag := nr.GatherPaginated(nrTarget, apiKey)
	var bods []AllBody
	for i := range bag.Items {
		loopedBods := AllBody{
			Id:           bag.Items[i].Id,
			Appname:      bag.Items[i].Name,
			Healthstatus: bag.Items[i].HealthStatus,
			Enabled:      bag.Items[i].Reporting,
		}
		bods = append(bods, loopedBods)
	}
	encoded, _ := json.Marshal(bods)
	w.Write([]byte(encoded))
}
