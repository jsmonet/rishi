# Rishi - the monitoring outpost

Rishi's eventual goal is to pull from both NewRelic and DataDog to present a unified view of coverage. 

## NewRelic inputs

Api key: Account Settings -> Api Keys

Api explorer: https://rpm.newrelic.com/api/explore

Use this to generate queries, etc. This also shows the basic format of the curl statement. Let's use a general listing statement for an example:

```bash.
curl -X GET 'https://api.newrelic.com/v2/applications.json' -H 'X-Api-Key:{api_key}'
```

The canned curl statement from NR includes the -i flag which includes header data. Since we are parsing, we have no need for header data. In fact, it's a liability. 

The Api key is 47 characters and alphanumeric, no spaces or symbols. It's a hash. 

The header object is defined as `X-Api-Key`

We're using `http.NewRequest` to pull the data. 

## NewRelic Use

You can always get standard usage help by passing the `-h` flag:

`rishi -h`

### Application list

`rishi -p n -a $APIKEY`

Example of the json response:
```json
{
  "applications": [
    {
      "id": 74733802,
      "name": "ensemble-api-production",
      "language": "java",
      "health_status": "green",
      "reporting": true,
      "last_reported_at": "2018-08-01T16:20:44+00:00",
      "application_summary": {
        "response_time": 49.2,
        "throughput": 273,
        "error_rate": 0.0183,
        "apdex_target": 0.5,
        "apdex_score": 0.98,
        "host_count": 3,
        "instance_count": 3
      },
      "settings": {
        "app_apdex_threshold": 0.5,
        "end_user_apdex_threshold": 7,
        "enable_real_user_monitoring": true,
        "use_server_side_config": false
      },
      "links": {
        "application_instances": [
          157458177,
          157458298,
          157827595
        ],
        "servers": [],
        "application_hosts": [
          157458176,
          157458297,
          157827594
        ]
      }
    }
  ]
}
```

Example of the rishi output:

```shell
ID: 9999998	Name: stg-xfh-api_domain-i-be09999e	 Status: gray	 IsReporting?: true
ID: 9999994	Name: stg-xfh-api_domain-i-d999999b	 Status: gray	 IsReporting?: true
ID: 9999991	Name: stg-xfh-api_domain-i-c9999992	 Status: gray	 IsReporting?: true
ID: 9999997	Name: stg-xfh-api_domain-i-d999999e	 Status: gray	 IsReporting?: true
```

### Show instances

`rishi.go -p newrelic -t showinstances -a $APIKEY --appid $APPID`

This consumes the apiKey as well as an appid, which you find in the app list output at:
```json
{
  "applications": [
    {
      "id": this int right here
    }
  ]
}
```

Example of the json response:
```json
{
  "application_instances": [
    {
      "id": 157458177,
      "application_name": "ensemble-api-production",
      "host": "ensemble-api-247863736-1nzjp",
      "language": "java",
      "health_status": "green",
      "application_summary": {
        "response_time": 22.5,
        "throughput": 19.9,
        "error_rate": 0.0067,
        "apdex_score": 0.99,
        "instance_count": 1
      },
      "links": {
        "application": 74733802,
        "application_host": 157458176
      }
    },
    {
      "id": 157458298,
      "application_name": "ensemble-api-production",
      "host": "ensemble-api-247863736-kcr3q",
      "language": "java",
      "health_status": "green",
      "application_summary": {
        "response_time": 38.8,
        "throughput": 19.5,
        "error_rate": 0.0051,
        "apdex_score": 0.98,
        "instance_count": 1
      },
      "links": {
        "application": 74733802,
        "application_host": 157458297
      }
    },
    {
      "id": 157827595,
      "application_name": "ensemble-api-production",
      "host": "ensemble-api-247863736-22cff",
      "language": "java",
      "health_status": "green",
      "application_summary": {
        "response_time": 32.1,
        "throughput": 19.2,
        "error_rate": 0.0052,
        "apdex_score": 0.98,
        "instance_count": 1
      },
      "links": {
        "application": 74733802,
        "application_host": 157827594
      }
    }
  ]
}
```

This might actually be a poor use of code with relation to NewRelic, because the core use of NewRelic is synthetics, which are handled below. 

### Validate (soft) NewRelic server checks

`rishi.go -p newrelic -t validatenr -a $APIKEY`

This is noted (soft) because the conditions used to 'validate' are Exists + Has child elements. This is not the most reliable metric.

Example the rishi output:
```shell
99856624 named shield-services-int has no instances
100193169 named shield-services-prd has no instances
100113896 named shield-services-stg has no instances
106190643 named shield-services-tpn has no instances
```

In this case, this account only has synthetics checking via NewRelic.

```shell
16885969 named AzkabanExecutor-Prod has instance id: 157798501 named ensemble-web-dev
10310141 named Beverly Keychest has instance id: 161324652 named ensemble-web-dev
9535408 named ChannelDCVI has instance id: 161325093 named ensemble-web-dev
```

In this case there are pods associated with checks on this account. Individuals will not always have names

### List Synthetics Policies

`go run cmd/rishi.go -p newrelic -t synthpolicies -a $APIKEY`

This consumes the apiKey and returns the ID and Name of each Synthetics policy. The ID from this is recycled in the function below to list the information about the individual checks

Example of the json response:
```json
{
  "policies": [
    {
      "id": 180188,
      "incident_preference": "PER_CONDITION",
      "name": "stg-shield",
      "created_at": 1516763917910,
      "updated_at": 1516925244211
    },
    {
      "id": 182558,
      "incident_preference": "PER_CONDITION",
      "name": "prd-shield",
      "created_at": 1516925207194,
      "updated_at": 1516925252759
    }
  ]
}
```

Example of the rishi output:
```shell
Id:	180188	Name:	stg-shield
Id:	182558	Name:	prd-shield
```

### Show all Synthetics conditions by policy

`go run cmd/rishi.go -p newrelic -t synthconditions -a $APIKEY`

This consumes the apiKey and returns lists of the individual synthatics checks by consumer. 

Example of the json response:
```json
{
  "synthetics_conditions": [
    {
      "id": 798556,
      "name": "prd-shield-ui-health",
      "monitor_id": "5c1f2b68-d5a3-48df-a098-37e17d93c2b4",
      "runbook_url": "https://tech.studio.disney.com/wiki/display/SE/Shield",
      "enabled": true
    },
    {
      "id": 798557,
      "name": "prd-shield-vendorportal-health",
      "monitor_id": "e5d974cc-184d-417c-87c6-63b0073b6c0a",
      "runbook_url": "https://tech.studio.disney.com/wiki/display/SE/Shield",
      "enabled": true
    }
  ]
}
```

Example of rishi output:
```shell
180188 - stg-shield:
Id:	798148	Name:	stg-shield-ui-health	Monitor ID:	ac37db50-8beb-4bf8-8ab8-faf0796d7ce8	Enabled:	true
Id:	798176	Name:	stg-shield-vendorportal-health	Monitor ID:	151c568e-fef3-4330-a5ed-84e69932997e	Enabled:	true
Id:	798177	Name:	stg-shield-vendorroster-health	Monitor ID:	dcb45292-d2bd-4c09-b04a-d12b4eee32c8	Enabled:	true
Id:	798464	Name:	stg-shield-moodle-health	Monitor ID:	c5b4436c-de37-4e26-ad29-85a39b1f6757	Enabled:	true
```

    
## NewRelic Query construction

__Conventions__

In json examples, `"...": ...` simply represents any value. This should be regarded as a placeholder with no other meaning

__Components__

Constructing the curl request for NR requires a certain amount of modularity. Below I'll detail how I broke this out. 

`Core` - Data: `https://api.newrelic.com/v2/applications` This is the core address from which everything stems

`List` - Data: `.json` Adding this extension returns the applicable json list output

`Paginate` - Data: `?page=` This requires a secondary variable set to a number, starting at `1`. This program uses the ambiguous `i` for that placeholder. 

`Page number` - Data: `1, incrementing in a loop` This, as mentioned above, is designated by the ambiguous `i` and used in a loop until new page returns contain no valid data. 

`App ID` - Data: when you list the app instances, this is found at `{"applications": [{"id": integer, "...": ...}]}` The element `"id": integer` provides this value. 

`ID` - Data: data returned from the application list query gives you `{"application_instances": [{"id": integer, "...": ...}]}` The element `"id": integer` provides this value. You use this value to create your show application query


## goals for alpha

- [x] List all applications, no filters
- [x] Refactor for organization, add functions and structs to handle application instance listing and show app capabilities
- [x] Use app instance listing and views to validate larger list
- [ ] Reuse existing structures to optionally provide health/metrics output
- [ ] Add DataDog module

