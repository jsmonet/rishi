package nr

import (
	"encoding/json"
	"fmt"
	"gitlab.disney.com/Joseph.E.Franco.-ND/rishi/pkg/core"
)

// Response is a struct that explicitly accesses the nested json element 'Applications', which is an array. The parent element, 'applications', is also an array--which is why it is sliced here.
type Response struct {
	Applications []struct {
		Id int `json:"id"`
		Name string `json:"name"`
		HealthStatus string `json:"health_status"`
		Reporting bool `json:"reporting"`
	} `json:"applications"`
}

// likewise simplified struct where the aggregated data goes
type Destination struct {
	Id int
	Name string
	HealthStatus string
	Reporting bool
}

// struct to contain sliced of Destination because it needs to be able to handle multiple identical series' of keys
type DataBag struct {
	Items []Destination
}

// GatherPaginated takes a base target of the NewRelic api url and a string containing your NR api key. It pulls the result from each http.DefaultClient.Do(req) run into the resultArray variable, which points to the Destination struct.
func GatherPaginated(target string, apiKey string) (outer DataBag) {

	pageTarget := target + "/applications.json?page="
	i := 1

	inner := []Destination{}
	outer = DataBag{inner}


	goOn := true

	for goOn {

		paginatedTargetUrl := fmt.Sprintf("%v%v", pageTarget, i)

		rawBody := core.Grab(paginatedTargetUrl, apiKey)

		parsed := toJsonListResponse(rawBody)

		if len(parsed.Applications) == 0 { // check the array length instead
			goOn = false
		} else {
			for i := range parsed.Applications {
				loopData := Destination{
					Id: parsed.Applications[i].Id,
					Name: parsed.Applications[i].Name,
					HealthStatus: parsed.Applications[i].HealthStatus,
					Reporting: parsed.Applications[i].Reporting,
				}
				outer.appItems(loopData) // add slice of 'inner' to struct 'outer'
			}
		}
		// advance the counter
		i++
	}
	// end of 'for goOn' loop

	return
}

// toJsonListResponse is a local func that unmarshals []byte from a gather wrapper function to feed a Response struct var
func toJsonListResponse(rawBody []byte) (parsedBody Response) {
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}
	return
}

// appItems is a local func that appends the Databag struct var, slice by slice. The name is short for append Items, not application Items. Disambiguation may follow
func (bag *DataBag) appItems(items Destination) []Destination {
	bag.Items = append(bag.Items, items)
	return bag.Items
}

// ListAppsStdOut is an exported func that prints the result of func GatherPaginated to stdout
func ListAppsStdOut(bag DataBag, OnlyNoNames bool) {
	for i := range bag.Items {
		if OnlyNoNames{
			if len(bag.Items[i].Name) == 0 {
				fmt.Printf("AppID: %v\tName: %v\t Status: %v\t IsReporting?: %v\n", bag.Items[i].Id, bag.Items[i].Name, bag.Items[i].HealthStatus, bag.Items[i].Reporting)
			} else {

			}
		} else {
			fmt.Printf("AppID: %v\tName: %v\t Status: %v\t IsReporting?: %v\n", bag.Items[i].Id, bag.Items[i].Name, bag.Items[i].HealthStatus, bag.Items[i].Reporting)
		}
	}
	return
}