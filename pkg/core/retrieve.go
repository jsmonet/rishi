package core

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Grab(target string, apiKey string) (rawBody []byte) {
	req, _ := http.NewRequest("GET", target, nil)
	req.Header.Set("X-Api-Key", apiKey)
	req.Header.Set("Content-Type", "application/json")

	res, _ := http.DefaultClient.Do(req)
	rawBody, bodyreaderr := ioutil.ReadAll(res.Body)
	if bodyreaderr != nil {
		log.Println(bodyreaderr)
	}
	res.Body.Close()
	return
}

func ToJson(rawBody []byte) interface{} {
	var parsedBody interface{}
	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return parsedBody
}

func GetCode(target string) (statusCode int) {
	response, err := http.Get(target)
	if err != nil {
		log.Fatal(err)
	}

	statusCode = response.StatusCode

}
