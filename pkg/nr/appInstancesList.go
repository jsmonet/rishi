package nr

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/jsmonet/rishi/pkg/core"
)

// reusable inner struct
type AppInstNest struct {
	Id           int    `json:"id"`
	AppName      string `json:"application_name"`
	Host         string `json:"host"`
	HealthStatus string `json:"health_status"`
}

// AppSingleInstance may not be of any use
type AppSingleInstance struct {
	NestFlds []AppInstNest `json:"application_instance"`
}

type AppListInstances struct {
	NestFlds []AppInstNest `json:"application_instances"`
}

// AiGather consumes a target string, apiKey string, and an application ID int and produces an AppListInstances struct. It sends the call off and unmarshalls the raw response data, depositing it into a var that fits the AppListInstances struct spec. This func is largely a wrapper function.
func AiGather(target string, apiKey string, appID int) (outer AppListInstances) {

	appInstanceTarget := fmt.Sprintf("%v/applications/%v/instances.json", target, appID)
	//fmt.Println(appInstanceTarget)
	rawBody := core.Grab(appInstanceTarget, apiKey)
	outer = ToJsonAiResponse(rawBody)

	return

}

// ToJsonAiResponse consumes a raw []byte http request result body and returns a struct var based on the struct AppListInstances
func ToJsonAiResponse(rawBody []byte) (parsedBody AppListInstances) {

	err := json.Unmarshal(rawBody, &parsedBody)
	if err != nil {
		fmt.Println(err)
	}

	return
}

// ShowInstancesStdOut pretty-prints the output to stdout.
func ShowInstancesStdOut(bag AppListInstances) {
	for i := range bag.NestFlds {
		fmt.Printf("ID:\t%v\tHost:\t%v\tAppName:\t%v\tHealth Status:\t%v\n", bag.NestFlds[i].Id, bag.NestFlds[i].Host, bag.NestFlds[i].AppName, bag.NestFlds[i].HealthStatus)
	}
	return
}
